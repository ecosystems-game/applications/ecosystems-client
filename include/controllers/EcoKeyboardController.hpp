//#ifndef ECOSYSTEMS_ECOKEYBOARDCONTROLLER_HPP
//#define ECOSYSTEMS_ECOKEYBOARDCONTROLLER_HPP
//
//#include "system/InputController.hpp"
//#include "system/GameView.hpp"
//#include <iostream>
//#include <memory>
//
//
//namespace ecosystems::client::controllers {
//    class EcoKeyboardController : public system::InputController{
//    public:
//        struct KeyMappings {
//            int moveLeft = GLFW_KEY_D;
//            int moveRight = GLFW_KEY_A;
//            int moveForward = GLFW_KEY_W;
//            int moveBackward = GLFW_KEY_S;
//            int moveUp = GLFW_KEY_SPACE;
//            int moveDown = GLFW_KEY_LEFT_SHIFT;
//            int lookLeft = GLFW_KEY_LEFT;
//            int lookRight = GLFW_KEY_RIGHT;
//            int lookUp = GLFW_KEY_UP;
//            int lookDown = GLFW_KEY_DOWN;
//        };
//
//        struct ControlStates {
//            bool lookRight = false;
//            bool lookLeft = false;
//            bool lookUp = false;
//            bool lookDown = false;
//            bool moveRight = false;
//            bool moveLeft = false;
//            bool moveForward = false;
//            bool moveBackward = false;
//            bool moveDown = false;
//            bool moveUp = false;
//        };
//
//        void onKeyDown(int p_key, int p_scancode, int p_mods) override;
//        void onKeyUp(int p_key, int p_scancode, int p_mods) override;
//        void onMouseButtonUp(int p_button, int p_mods, glm::fvec2 p_mouse_position) override;
//        void onMouseButtonDown(int p_button, int p_mods, glm::fvec2 p_mouse_position) override;
//        void onCursorMove(double p_xpos, double p_ypos, glm::fvec2 p_mouse_position) override;
//
//        KeyMappings keys{};
//        ControlStates states{};
//        float moveSpeed{3.f};
//        float lookSpeed{1.5f};
//    };
//};
//
//#endif //ECOSYSTEMS_ECOKEYBOARDCONTROLLER_HPP
