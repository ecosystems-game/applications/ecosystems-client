#ifndef ECOSYSTEMS_MAINGAMESCENE_HPP
#define ECOSYSTEMS_MAINGAMESCENE_HPP

#include "data-objects/GraphicsObjects.hpp"

#include <eco-system/Scene.hpp>
#include <eco-graphics/Device.hpp>
#include <eco-system/Entity.hpp>

#include <iostream>

namespace ecosystems::client::views {
    class MainGameScene : public system::Scene {
    public:
        explicit MainGameScene(data_objects::GraphicsObjects& p_graphics_objects);

        ~MainGameScene() {
            std::cout << "Destroying Main Game Scene" << std::endl;
        }
        void mounted() override;
        void update(float p_delta_time) override;

        // graphics::Camera& get_camera() { return m_camera; }
        //std::unique_ptr<InputController>& get_input_controller() { return m_input_controller; }
        //void set_input_controller(InputController* p_input_controller) { m_input_controller.reset(p_input_controller); }
    private:
        void loadGameObjects();

        data_objects::GraphicsObjects& m_graphics_objects;
        system::Entity m_model, m_camera;
    };
}

#endif //ECOSYSTEMS_MAINGAMESCENE_HPP
